" Display signs on QuickFix result lines
" Maintainer: Hauleth <lukasz@niemier.pl>
" License: MIT

if exists('g:loaded_qfx')
    finish
endif
let g:loaded_qfx = 1

let s:save_cpo = &cpo
set cpo&vim

let g:qfx_sign = get(g:, 'qfx_sign', ' ')
let g:qfx_hl_err = get(g:, 'qfx_hl_err', 'ErrorMsg')
let g:qfx_hl_warn = get(g:, 'qfx_hl_warn', 'Cursor')
let g:qfx_hl_info = get(g:, 'qfx_hl_info', 'Search')

execute 'sign define QFxErr  texthl=' . g:qfx_hl_err  . ' text=' . g:qfx_sign
execute 'sign define QFxWarn texthl=' . g:qfx_hl_warn . ' text=' . g:qfx_sign
execute 'sign define QFxInfo texthl=' . g:qfx_hl_info . ' text=' . g:qfx_sign

command! -nargs=0 -bar QFxClear call qfx#clear()
command! -nargs=0 -bar QFxPlace call qfx#place()

augroup qfx
    autocmd!
    autocmd QuickFixCmdPre [^l]* QFxClear
    autocmd QuickFixCmdPost [^l]* QFxPlace

    autocmd BufWinLeave * if getbufvar(0 + expand('<abuf>'), '&ft') ==? 'qf' | QFxClear | endif
    autocmd BufWinEnter * if getbufvar(0 + expand('<abuf>'), '&ft') ==? 'qf' | QFxPlace | endif
augroup END

let &cpo = s:save_cpo
unlet s:save_cpo
